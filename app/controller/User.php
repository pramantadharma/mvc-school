<?php
    
class User extends Controller {

    public function __construct() {
        if (!empty($_SESSION['user_id'])) {
                header("Location: ". BASE_URL . "/home/about");
            }
    }

    public function index() {
        $this->view('auth/register');
    }

    public function store() {

        // Validasi
        if (condition) {
            # code...
        }

        $duplicateEmail = $this->model('User_model')->findUserbyEmailorUsername($_POST['emailorusername']);
        if ($duplicateEmail) {
            Flasher::setFlash("Maaf, akun sudah terdaftar !", "", "danger");
            header("Location: ". BASE_URL . "/user/register");
            exit;
        } else {
            if($this->model('User_model')->createUser($_POST) > 0) {
                // echo "<script>
                // alert('Data bertambah!');
                // document.location.href = 'login.php'
                // </script>";
                // exit;
                Flasher::setFlash('Berhasil', 'added', 'success');
                header('Location: ' . BASE_URL. '/user/login');
                exit;
            }
        } 
    }

    public function login() {
        $data['message'] = '';
        $this->view('auth/login', $data);
    }

    public function storeLogin() {

        if (empty($_POST['emailorusername'])) {
            Flasher::setFlash("Please fill your email", "", "danger");
            header('Location: ' . BASE_URL . "/user/login");
        } elseif(empty($_POST['login'])) {
            Flasher::setFlash("Please fill your password", "", "danger");
            header('Location: ' . BASE_URL . "/user/login");
        }
        
        if ($this->model('User_model')->findUserbyEmailorUsername($_POST['emailorusername'])) {
            $loginUser = $this->model('User_model')->login($_POST);
            
            if ($loginUser) {
                ($this->model("User_model")->createSession($loginUser));
            } else {
                Flasher::setFlash("Password Incorrect", "", "danger");
                header('Location: ' . BASE_URL . "/user/login");
                exit;
                // $data['message'] = 'Password Incorrect';
                // $this->view('auth/login', $data);
            }
        } else {
            Flasher::setFlash("Account not found", "", "danger");
            header('Location: ' . BASE_URL . "/user/login");
            exit;
            // $data['message'] = 'Account not Found';
            // $this->view('auth/login', $data);
        }

    }

    public function logout() {
        $_SESSION = [];
        header("Location: ". BASE_URL . "/user/login");
    }

}

?>