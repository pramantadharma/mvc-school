<?php

class Home extends Controller{

    public function __construct() {
        if(empty($_SESSION['user_id'])){
            header("Location: ". BASE_URL . "/user/login");
        }
    }

    public function index(){
        $data['title'] = 'Home';
        $data['user'] = $this->model('User_model')->getAllUser();
        $this->view("templates/header", $data);
        $this->view("home/index", $data);
        $this->view("templates/footer", $data);
    }
    
    public function about($company = "SMK 1"){
        $data['username'] = $_SESSION['username'];
        $data['user'] = $this->model('User_model')->getAllUser();
        $data['title'] = "About";
        $data['company'] = ucwords($company);
        $this->view("templates/header", $data);
        $this->view("home/about", $data);
        $this->view("templates/footer", $data);
    }

    public function create() {
        if($this->model('User_model')->createUser($_POST) > 0) {
            // echo "<script>
            // alert('yahahaha data bertambah dude!');
            // document.location.href = 'about.php'
            // </script>";
            // exit;
            header('Location: ' . BASE_URL. '/home/about');
            exit;
        } else {
            echo("error");
            exit;
        }
    }
}

?>