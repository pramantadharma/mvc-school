
<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="../../../public/bootstrap/css/bootstrap.min.css">
    <style>
        .mid {
            text-align : center;
        }
    </style>
</head>
<body>
    <div class="text-center">
        <h1 class="mid">Mozzart</h1>
        <h1 class=""></h1>
    </div>
</body>
</html> -->


<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <button type="button" class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah Data Baru
        </button>
    </div>



    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
    <?php Flasher::flash() ?>
      <h3>Welcome <?= isset($data['username']) ? $data['username'] : $data['company']  ?>✌🤩</h3>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">Username</th>
              <th scope="col">Email</th>
              <th scope="col">Name</th>
              <th scope="col" class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($data['user'] as $usr) {
            ?>
            <tr>
              <td><?= $usr['id'] ?></td>
              <td><?= $usr['username'] ?></td>
              <td><?= $usr['email'] ?></td>
              <td><?= $usr['first_name'] . " " . $usr['last_name'] ?></td>
              <td>
                <div class="d-flex gap-2 justify-content-center">
                    <button class="btn btn-success">Edit</button>
                    <button class="btn btn-danger">Delete</button>
                </div>
              </td>
            </tr>
            <?php
            };
            ?>
          </tbody>
        </table>
    </div>
</main>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="<?= BASE_URL ?>/home/create" method="post" >
                        <div class="mb-3">
                            <label for="username" class="col-form-label">Username:</label>
                            <input type="text" name="username" class="form-control" id="username">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="col-form-label">Email:</label>
                            <input type="email" name="email" class="form-control" id="email">
                        </div>
                        <div class="mb-3">
                            <label for="first_name" class="col-form-label">First Name:</label>
                            <input type="text" name="first_name" class="form-control" id="first_name">
                        </div>
                        <div class="mb-3">
                            <label for="last_name" class="col-form-label">Last Name:</label>
                            <input type="text" name="last_name" class="form-control" id="last_name">
                        </div>
                        <div class="mb-3">
                            <label for="password" class="col-form-label">Password:</label>
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                        <div class="mb-3">
                            <label for="password2" class="col-form-label">Confirm Password:</label>
                            <input type="password" name="password2" class="form-control" id="password2">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-dark">Tambah Data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

 