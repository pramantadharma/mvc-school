
<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="../../../public/bootstrap/css/bootstrap.min.css">
    <style>
        .mid {
            text-align : center;
        }
    </style>
</head>
<body>
    <div class="text-center">
        <h1 class="mid">Mozzart</h1>
        <h1 class=""></h1>
    </div>
</body>
</html> -->

<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar" class="align-text-bottom"></span>
            This week
          </button>
        </div>
      </div>

      <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

      <h2>Welcome <?= isset($data['company']) ?  $data['company'] : '' ?> ✌🤩</h2>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">Username</th>
              <th scope="col">Email</th>
              <th scope="col">First Name</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($data['user'] as $usr) {
            ?>
            <tr>
              <td><?= $usr['id'] ?></td>
              <td><?= $usr['username'] ?></td>
              <td><?= $usr['email'] ?></td>
              <td><?= $usr['first_name'] ?></td>
              <td>placeholder</td>
            </tr>
            <?php
            };
            ?>
          </tbody>
        </table>
      </div>
    </main>