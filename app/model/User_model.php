<?php
class User_model {
    private $table = 'users';
    private $db;
    private $name = 'Prama';

    public function getUser() {
        return $this->name;
    }

    public function __construct() {
        $this->db = new Database();
    }

    public function getAllUser() {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }

    public function getUserById($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id' , $id);
        return $this->db->resultSingle();
    }

    public function createUser($data) {
        $username = htmlspecialchars($data['username']);
        $email = htmlspecialchars($data['email']);
        $first_name = htmlspecialchars($data['first_name']);
        $last_name = htmlspecialchars($data['last_name']);
        $password = $data['password'];
        $password2 = $data['password2'];

        // if ($this->findUserbyEmail($email) > 0) {
        //     header("Location: ". BASE_URL . "/auth/")
        // }

        if($password === $password2) {
            $password = password_hash($data['password'], PASSWORD_BCRYPT);

            // $query = "INSERT INTO {$this->table} VALUES (0, :username, :email, :first_name, :last_name, :password, '')";

            $this->db->query("INSERT INTO {$this->table}  
                            VALUES (0, :username, :email, :first_name, :last_name, :password, '')");

            // $this->db->query($query);
            $this->db->bind('username', $username);
            $this->db->bind('email', $email);
            $this->db->bind('first_name', $first_name);
            $this->db->bind('last_name', $last_name);
            $this->db->bind('password', $password);
            
            $this->db->execute();
            return $this->db->rowCount();
        } else {
            echo "<script> 
            alert('Password tidak sama, mohon masukkan ulang');
            document.location.href = 'auth/register';
            </script>";
            // sleep(5);
            // header("Location: " . BASE_URL . "/user/index", true, 303);
            // header("Refresh: 5; url=" . BASE_URL . "/user/index", true, 300);
            exit;
        }
    }

    public function findUserbyEmailorUsername($data) {
        $this->db->query("SELECT * FROM {$this->table} WHERE email = :email OR username = :username");

        $this->db->bind('email', $data);
        $this->db->bind('username', $data);
        $row = $this->db->resultSingle();

        if ($row > 0 ) {
            return $row;
        } else {
            return false;
        }
    }

    public function login($data) {

        $row = $this->findUserbyEmailorUsername($data['emailorusername']);

        if ($row == false) {
            return false;
        }
        
        $hashPass = $row['password'];
        if (password_verify($data['password'], $hashPass)) {
            return $row;
            
        } else {
            return false;
        }
    }

    public function createSession($user) {
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['email'] = $user['email'];

        // $_SESSION['user'] = ['id', 'username', 'email'];
        
        Flasher::setFlash('Berhasil Login', '', 'success');
        header("Location: " . BASE_URL . "/home/about");
        exit;
    }
}
?>